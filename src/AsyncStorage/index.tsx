import AsyncStorage from "@react-native-async-storage/async-storage"

export const USER_LOGIN = 'USER_LOGIN'

export async function load(key: string): Promise<any | null> {
    try {
        const almostThere: any = await AsyncStorage.getItem(key)
        return JSON.parse(almostThere)
    } catch {
        return null
    }
}


export async function save(key: string, value: any): Promise<boolean> {
    try {
        await AsyncStorage.setItem(key, JSON.stringify(value))
        return true
    } catch {
        return false
    }
}