import { actionTypes } from '../actions/UserActions'

interface Actions {
    payload: any
    type: any
}

const initialState = {
    isGoHome: false
}


const userReducer = (state = initialState, action: Actions) => {
    const { payload, type } = action
    switch (type) {
        case actionTypes.USER_LOGIN_SUCCESS:
            return {
                ...state,
                ...payload.user
            }

        case actionTypes.GO_HOME:
            return {
                ...state,
                isGoHome: !state.isGoHome
            }

        case actionTypes.LOGOUT:
            return null

        default:
            return state
    }
};

export default userReducer