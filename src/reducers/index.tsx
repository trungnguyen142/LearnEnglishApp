import { AnyAction, combineReducers, EmptyObject } from 'redux';
import user from './UserReducer'

const appReducer = combineReducers({ user });

export default (state: EmptyObject | any, action: AnyAction | any) => {


    return appReducer(state, action)
};
