import client, { setClientToken } from "./Client"

const Users = {
    async SignIn(email: string, password: string) {
        try {
            const res = await client.post(`api/login`, { email, password })
            setClientToken(res?.data)
            return res
        } catch (err) {
            throw err
        }
    },

    async SignUp(params: any) {
        try {
            const res = await client.post(`api/register`, { ...params })
            setClientToken(res?.data)
            return res
        } catch (err) {
            throw err
        }
    },

    async logout() {
        return new Promise(resolve => {
            setClientToken(null)
            setTimeout(resolve, 1000)
        });
    }
}

export default Users