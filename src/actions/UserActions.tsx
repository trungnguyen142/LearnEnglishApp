
import Users from "@/controllers/User"

export const actionTypes = {
    GO_HOME: 'GO_HOME',

    USER_LOGIN: 'USER_LOGIN',
    USER_LOGIN_REQUEST: 'USER_LOGIN_REQUEST',
    USER_LOGIN_SUCCESS: 'USER_LOGIN_SUCCESS',
    USER_LOGIN_ERROR: 'USER_LOGIN_ERROR',

    USER_REGISTER: 'USER_REGISTER',
    USER_REGISTER_SUCCESS: 'USER_REGISTER_SUCCESS',
    USER_REGISTER_REQUEST: 'USER_REGISTER_REQUEST',
    USER_REGISTER_ERROR: 'USER_REGISTER_ERROR',

    LOGOUT: 'LOGOUT',


}

export const Go_to_home = () => ({
    type: actionTypes.GO_HOME
})

const loginRequest = () => ({
    type: actionTypes.USER_LOGIN_REQUEST,
    payload: null,
});

const loginError = (error: any) => ({
    type: actionTypes.USER_LOGIN_ERROR,
    payload: { error },
});

const loginSuccess = (user: any) => ({
    type: actionTypes.USER_LOGIN_SUCCESS,
    payload: { user },
});

const registerRequest = () => ({
    type: actionTypes.USER_REGISTER_REQUEST,
    payload: null,
});

const registerError = (error: any) => ({
    type: actionTypes.USER_REGISTER_ERROR,
    payload: { error },
});

const registerSuccess = (user: any) => ({
    type: actionTypes.USER_REGISTER_SUCCESS,
    payload: { user },
})

const logoutRequest = () => ({
    type: actionTypes.LOGOUT,
    payload: null,
})

export const userLogin = (email: string, password: string) => async (dispatch: (arg0: { type: string; payload: { error: any; } | { user: any; } | null; }) => void) => {
    dispatch(loginRequest())
    try {
        const user = await Users.SignIn(email, password)
        dispatch(loginSuccess(user));
        return user
    } catch (error) {
        dispatch(loginError(error.message));
        throw error;
    }
}

export const userRegister = (email: string, password: string, password_confirmation: string) => async (dispatch: (arg0: { type: string; payload: { error: any; } | { user: any; } | null; }) => void) => {
    dispatch(registerRequest())
    try {
        const user = await Users.SignUp({ email, password, password_confirmation })
        dispatch(registerSuccess(user))
        return user
    } catch (error) {
        dispatch(registerError(error.message))
        throw error
    }
}

export const userLogout = () => (dispatch: (arg0: { type: string; payload: null; }) => void) => {
    Users.logout()
    dispatch(logoutRequest())
}