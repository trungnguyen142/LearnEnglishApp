import Metrics from "./Metrics"
import Colors from './Colors'
export const FontFamily = {
    BeVietnamPro: {
        Bold: 'BeVietnamPro-Bold',
        Light: 'BeVietnamPro-Light',
        Medium: 'BeVietnamPro-Medium',
        Regular: 'BeVietnamPro-Regular',
    }
}

export const Typography = {
    h1: {
        fontFamily: FontFamily.BeVietnamPro.Bold,
        fontSize: Metrics.fontSize.h1,
        color: Colors.BLACK
    },
    h2: {
        fontFamily: FontFamily.BeVietnamPro.Medium,
        fontSize: Metrics.fontSize.h2,
        color: Colors.BLACK
    },
    h3: {
        fontFamily: FontFamily.BeVietnamPro.Regular,
        fontSize: Metrics.fontSize.h3,
        color: Colors.BLACK
    },
    SemiSmall: {
        fontFamily: FontFamily.BeVietnamPro.Regular,
        fontSize: Metrics.fontSize.h4,
        color: Colors.BLACK
    },
    Small: {
        fontFamily: FontFamily.BeVietnamPro.Regular,
        fontSize: Metrics.fontSize.small,
        color: Colors.BLACK
    },

} 
