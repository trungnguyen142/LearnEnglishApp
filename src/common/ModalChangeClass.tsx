import { Platform, StyleSheet, View, TouchableOpacity } from 'react-native'
import React, { useState } from 'react'
import Modal from 'react-native-modal'
import Colors from './Colors'
import VText from './VText'
import GradientButton from './GradientButton'
import { actuatedNormalize } from './Metrics'
import FlexRow from './FlexRow'
import CardView from 'react-native-cardview'
import LButton from './LButton'
import IButton from './IButton'
import Images from '@/assets/images/BgButton'

interface ModalProps {
    isVisible: boolean
    onCloseModal?: () => void
    onPress?: any
}

const ModalChangeClass = (props: ModalProps) => {
    const { isVisible, onCloseModal, onPress } = props
    const [currentIndex, setCurrentIndex] = useState(0)
    return (
        <Modal
            isVisible={isVisible}
            onBackdropPress={onCloseModal}
            backdropOpacity={0.1}
            useNativeDriver={true}
            useNativeDriverForBackdrop={true}
            hideModalContentWhileAnimating={true}
            style={styles.modal}
        >
            <View style={styles.containerModal}>
                <VText textStyle={styles.title}>
                    Chọn lớp
                </VText>
                <FlexRow style={styles.wrap}>
                    {
                        [...Array(5).keys()].map((item, index) => {
                            let activeIndex = false
                            if (index === currentIndex) {
                                activeIndex = true
                            }
                            if (activeIndex) {
                                return (
                                    <CardView
                                        key={`-${item}-${index}`}
                                        cardElevation={2}
                                        cardMaxElevation={1}
                                        cornerRadius={5}
                                        style={styles.btnPickClass}
                                    >
                                        <GradientButton
                                            onPress={() => {
                                                setCurrentIndex(index)
                                            }}
                                            color1='#0052D4'
                                            color2='#3C97FF'
                                            buttonStyle={{ flex: 1 }}
                                            textStyle={styles.txtPickClass}
                                            text={`Lớp ${item + 1}`}
                                        />
                                    </CardView>
                                )
                            }
                            else {
                                return (
                                    <CardView
                                        key={`-${item}-${index}`}
                                        cardElevation={0.5}
                                        cardMaxElevation={1}
                                        cornerRadius={5}
                                        style={[styles.btnPickClass, { backgroundColor: 'white', alignItems: 'center', justifyContent: 'center' }]}
                                    >
                                        <TouchableOpacity
                                            onPress={() => {
                                                setCurrentIndex(index)
                                            }}
                                            style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
                                        >
                                            <VText textStyle={[styles.txtPickClass, { color: 'black' }]}>
                                                {`Lớp ${item + 1}`}
                                            </VText>
                                        </TouchableOpacity>
                                    </CardView>
                                )
                            }
                        })
                    }
                </FlexRow>
                {/* <CardView
                    cardElevation={2}
                    cardMaxElevation={1}
                    cornerRadius={5}
                    style={styles.btnSubmit}
                >
                    <GradientButton
                        onPress={() => onPress(currentIndex + 1)}
                        color1='#EC6665'
                        color2='#EF924C'
                        buttonStyle={{ flex: 1 }}
                        textStyle={styles.txtPickClass}
                        text={`Xác nhận`}
                    />
                </CardView> */}
                <LButton
                    buttonStyle={styles.btnSubmit}
                    onPress={() => onPress(currentIndex + 1)}
                    text={`Xác nhận`}
                    source={Images.bg_button_blue}
                >

                </LButton>
            </View>
        </Modal>
    )
}

export default ModalChangeClass

const styles = StyleSheet.create({

    modal: {
        alignItems: 'center',
        justifyContent: 'flex-start',
        marginTop: actuatedNormalize(200)
    },

    title: {
        fontSize: actuatedNormalize(18),
        textAlign: 'center',
        fontWeight: Platform.OS === 'ios' ? '500' : '600'
    },

    wrap: {
        flexWrap: 'wrap',
        marginTop: actuatedNormalize(12),
        marginLeft: actuatedNormalize(8),
    },

    containerModal: {
        backgroundColor: Colors.WHITE_2,
        width: '80%',
        borderRadius: 20,
        paddingVertical: actuatedNormalize(16),
        // paddingLeft: actuatedNormalize(8)
    },

    btnPickClass: {
        width: actuatedNormalize(80),
        height: actuatedNormalize(40),
        borderRadius: actuatedNormalize(20),
        marginLeft: 8,
        marginBottom: 12
    },

    txtPickClass: {
        color: Colors.WHITE,
        fontWeight: Platform.OS === 'ios' ? '500' : '600'
    },

    btnSubmit: {
        width: actuatedNormalize(120),
        height: actuatedNormalize(48),
        marginBottom: actuatedNormalize(16),
        alignSelf: 'center',

    },

})