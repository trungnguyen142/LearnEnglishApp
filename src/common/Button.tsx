import { Text, TextStyle, TouchableOpacity } from 'react-native'
import React, { memo } from 'react'
import GradientText from './GradientText'
import { Typography } from './FontStyles'
import VText from './VText'

interface Props {
    onPress: () => void
    color1?: string
    color2?: string
    buttonStyle?: any
    textStyle?: TextStyle
    text: string | number
    isGradient?: boolean
}

const Button = (props: Props) => {
    const { onPress, color1, color2, buttonStyle, text, textStyle, isGradient } = props
    return (
        <TouchableOpacity
            onPress={onPress}
            style={buttonStyle}
            activeOpacity={0.5}
        >
            {
                isGradient ?
                    <GradientText style={textStyle} color1={color1} color2={color2}>
                        {text}
                    </GradientText>
                    :
                    <VText textStyle={textStyle}>
                        {text}
                    </VText>
            }

        </TouchableOpacity>
    )
}

export default memo(Button)