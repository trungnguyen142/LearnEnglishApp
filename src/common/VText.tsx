import { Text, StyleSheet } from 'react-native'
import React from 'react'
import { Typography } from './FontStyles'

interface FontStyle {
    textStyle?: any
    children: any
    propertys?: any
    numberOfLines?: number
    fontWeight?: string
    onLayout?: any
}

const VText = (props: FontStyle) => {
    const { children, textStyle, numberOfLines, fontWeight, onLayout, ...propertys } = props
    return (
        <Text {...propertys} onLayout={onLayout} numberOfLines={numberOfLines} style={[styles.text, textStyle, { fontWeight: fontWeight }]}>
            {children}
        </Text>
    )
}

const styles = StyleSheet.create({
    text: {
        ...Typography.h3,
        color: '#2A3447',
    },
})

export default VText