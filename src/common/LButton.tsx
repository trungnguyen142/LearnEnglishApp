import { TouchableOpacity, StyleSheet, ImageStyle, ImageSourcePropType, ImageBackground } from 'react-native'
import React, { memo } from 'react'
import BgButton from '@/assets/images/BgButton'
import VText from './VText'
import { actuatedNormalize, fontWeight } from './Metrics'

interface Props {
    onPress: () => void
    text?: string
    buttonStyle?: any
    source: ImageSourcePropType
    isChildren?: boolean
    children?: React.ReactElement | React.ReactElement[]
    style?: any
    imageStyle?: ImageStyle
}

const LButton = (props: Props) => {
    const { onPress, text, style, imageStyle, buttonStyle, source, isChildren, children } = props
    return (
        <TouchableOpacity
            activeOpacity={0.5}
            onPress={onPress}
            style={buttonStyle}
        >
            <ImageBackground
                imageStyle={imageStyle}
                style={[styles.btnSubmit, style]}
                source={source}>
                {
                    isChildren ?
                        children
                        :
                        <VText textStyle={{ color: 'white', fontWeight: fontWeight.SemiBold }}>
                            {text}
                        </VText>
                }

            </ImageBackground>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    btnSubmit: {
        width: actuatedNormalize(200),
        height: actuatedNormalize(48),
        marginTop: actuatedNormalize(18),
        marginBottom: actuatedNormalize(18),
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
})

export default memo(LButton)