import { WToast } from 'react-native-smart-tip'


export function WToastShowError(text: string) {
    const toastOpts = {
        data: text,
        textColor: '#ffffff',
        backgroundColor: '#E60A0A',
        duration: WToast.duration.LONG, //1.SHORT 2.LONG
        position: WToast.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
    }

    WToast.show(toastOpts)
}

export function WToastShowSuccess(text: string) {
    const toastOpts = {
        data: text,
        textColor: '#ffffff',
        backgroundColor: '#0052D4',
        duration: WToast.duration.LONG, //1.SHORT 2.LONG
        position: WToast.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
    }

    WToast.show(toastOpts)
}

