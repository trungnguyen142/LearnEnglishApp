import { Dimensions, Platform, PixelRatio, StatusBar } from 'react-native';

const { width, height } = Dimensions.get('window');

// based on iphone 11's scale (design)
const scale = width / 414;

export const actuatedNormalize = (size: number) => {
    const newSize = size * scale;
    if (Platform.OS === 'ios') {
        return Math.round(PixelRatio.roundToNearestPixel(newSize));
    } else {
        return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2;
    }
};

// check if devices is ipX, ipXS, ipXSMAx
const getPaddingTopByDevice = () => {
    let padTop = 0;
    if (Platform.OS === 'ios') {
        // check ipX, XS, XS max
        if (width === 812 || height === 812 || width === 896 || height === 896) {
            padTop = 24;
        }
    }
    return padTop;
};

// check if devices is ipX, ipXS, ipXSMAx
const getPaddingBottomByDevice = () => {
    let padBottom = 0;
    if (Platform.OS === 'ios') {
        // check ipX, XS, XS max
        if (width === 812 || height === 812 || width === 896 || height === 896) {
            padBottom = 25;
        }
    }
    return padBottom;
};

export const STATUSBAR_HEIGHT =
    Platform.OS === 'ios'
        ? 20 + getPaddingTopByDevice()
        : StatusBar.currentHeight;

export const PADDING_BOTTOM = getPaddingBottomByDevice();

const Metrics = {
    fontSize: {
        h1: actuatedNormalize(24),
        h2: actuatedNormalize(18),
        h3: actuatedNormalize(16),
        h4: actuatedNormalize(14),
        small: actuatedNormalize(12),
    },
    screenWidth: width,
    screenHeight: height,
};

export const fontWeight = {
    SemiBold: Platform.OS === 'ios' ? '600' : '700',
    Light: Platform.OS === 'ios' ? '500' : '600',
    Medium: Platform.OS === 'ios' ? '400' : '500',
}

export const isLoading = true
export const hideLoading = false

export default Metrics;
