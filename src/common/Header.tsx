import { StyleSheet, Text, View } from 'react-native'
import React, { useState, useEffect, memo } from 'react'
import SpaceBetween from './SpaceBetween'
import FlexRow from './FlexRow'
import CardView from 'react-native-cardview'
import GradientButton from './GradientButton'
import IButton from './IButton'
import VText from './VText'
import { actuatedNormalize, fontWeight } from './Metrics'
import Images from '@/assets/images/HomeIcon'
import Colors from './Colors'
import ModalChangeClass from '@/common/ModalChangeClass'

const Header = () => {
    const [currentClass, setCurrentClass] = useState(1)
    const [openModalChangeClass, setOpenModalChangeClass] = useState(false)

    return (
        <>
            <SpaceBetween style={{
                padding: 16,
                backgroundColor: 'white',
            }}>
                <FlexRow>
                    <CardView
                        cardElevation={1}
                        cardMaxElevation={1}
                        cornerRadius={5}
                        style={styles.cardClass}
                    >
                        <GradientButton
                            onPress={() => setOpenModalChangeClass(true)}
                            color1='#0052D4'
                            color2='#3C97FF'
                            buttonStyle={styles.btnPickClass}
                            textStyle={styles.txtPickClass}
                            text={`Lớp ${currentClass}`}
                        />
                    </CardView>
                    <CardView
                        cardElevation={1}
                        cardMaxElevation={1}
                        cornerRadius={5}
                        style={styles.cardNote}
                    >
                        <IButton
                            onPress={() => null}
                            source={Images.note}
                            buttonStyle={{}}
                            isImage={true}
                            direction="row"
                        >
                            <VText textStyle={{
                                fontSize: 14,
                                paddingLeft: 4,
                                fontWeight: fontWeight.SemiBold
                            }}>
                                7
                            </VText>
                        </IButton>
                    </CardView>

                    <CardView
                        cardElevation={1}
                        cardMaxElevation={1}
                        cornerRadius={5}
                        style={styles.cardNote}
                    >
                        <IButton
                            onPress={() => null}
                            source={Images.star}
                            buttonStyle={{}}
                            isImage={true}
                            direction="row"
                        >
                            <VText textStyle={{
                                fontSize: 14,
                                paddingLeft: 4,
                                fontWeight: fontWeight.SemiBold
                            }}>
                                1
                            </VText>
                        </IButton>
                    </CardView>
                </FlexRow>
                <CardView
                    cardElevation={1}
                    cardMaxElevation={1}
                    cornerRadius={5}
                    style={styles.cardNotifi}
                >
                    <IButton
                        onPress={() => null}
                        source={Images.notifi}
                        buttonStyle={{}}
                        isImage={true}
                    />
                </CardView>
            </SpaceBetween>
            <ModalChangeClass
                onPress={(value: number) => {
                    setCurrentClass(value)
                    setOpenModalChangeClass(false)
                }}
                isVisible={openModalChangeClass}
                onCloseModal={() => setOpenModalChangeClass(false)}
            />
        </>
    )
}

export default memo(Header)

const styles = StyleSheet.create({
    btnPickClass: {
        width: actuatedNormalize(64),
        height: actuatedNormalize(32),
        borderRadius: actuatedNormalize(20),
    },

    txtPickClass: {
        color: Colors.WHITE,
        fontWeight: '500',
    },

    notifiImage: {
        width: actuatedNormalize(30),
        height: actuatedNormalize(30),

    },

    cardNotifi: {
        width: actuatedNormalize(32),
        height: actuatedNormalize(32),
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
    },

    cardClass: {
        alignItems: 'center',
        justifyContent: 'center',
        width: actuatedNormalize(64),
        height: actuatedNormalize(32),
        borderRadius: actuatedNormalize(20),
    },

    cardNote: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        width: actuatedNormalize(64),
        height: actuatedNormalize(32),
        borderRadius: 20,
        marginLeft: actuatedNormalize(12),
    },

    cardItem: {
        borderRadius: 8,
        width: actuatedNormalize(160),
        marginRight: actuatedNormalize(16),
        backgroundColor: 'white',

    },

})