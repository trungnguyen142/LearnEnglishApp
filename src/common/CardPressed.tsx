import { View, Text } from 'react-native'
import React from 'react'
import CardView from 'react-native-cardview'
import LinearGradient from 'react-native-linear-gradient'
import { PADDING_BOTTOM } from './Metrics'

const CardPressed = () => {
    return (
        <CardView
            cardElevation={1}
            cardMaxElevation={1}
            cornerRadius={5}
            style={{
                width: '85%',
                height: 180,
                borderRadius: 8,
                backgroundColor: 'white',
                alignSelf: 'center',
                marginBottom: PADDING_BOTTOM + 40
            }}>
            <CardView
                cardElevation={2}
                cardMaxElevation={1}
                cornerRadius={5}
                style={{
                    backgroundColor: 'white',
                    alignSelf: 'center',
                    width: '110%',
                    height: 170,
                    borderRadius: 8,
                }}>
                <CardView
                    cardElevation={2}
                    cardMaxElevation={1}
                    cornerRadius={5}
                    style={{
                        backgroundColor: 'white',
                        alignSelf: 'center',
                        width: '110%',
                        height: 160,
                        borderRadius: 8,
                    }}>
                    <LinearGradient
                        colors={['#EC6665', '#EF924C']}
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 0 }}
                        style={{ flex: 1, borderRadius: 8, alignItems: 'center', justifyContent: 'center' }}
                    >

                    </LinearGradient>
                </CardView>
            </CardView>
        </CardView>
    )
}

export default CardPressed