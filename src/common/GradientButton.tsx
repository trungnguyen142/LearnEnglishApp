import { TouchableOpacity, TextStyle } from 'react-native'
import React, { memo } from 'react'
import VText from './VText'
import LinearGradient from 'react-native-linear-gradient'

interface Props {
    onPress: any
    color1?: any
    color2?: any
    buttonStyle?: any
    textStyle?: TextStyle
    text: React.ReactElement
}

const GradientButton = (props: Props) => {
    const { onPress, buttonStyle, color1, color2, text, textStyle } = props
    return (
        <TouchableOpacity
            activeOpacity={0.5}
            onPress={onPress}
            style={buttonStyle}
        >
            <LinearGradient
                colors={[color1, color2]}
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                style={{ flex: 1, borderRadius: 20, alignItems: 'center', justifyContent: 'center' }}
            >
                <VText textStyle={textStyle}>
                    {text}
                </VText>
            </LinearGradient>
        </TouchableOpacity>
    )
}

export default memo(GradientButton)