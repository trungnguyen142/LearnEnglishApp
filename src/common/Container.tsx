import { View, StyleSheet, ViewStyle } from 'react-native'
import React from 'react'

interface Props {
    children?: React.ReactElement | React.ReactElement[]
    style?: ViewStyle
}

const Container = (props: Props) => {
    const { children, style } = props
    return (
        <View style={[styles.container, style]}>
            {children}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
})

export default Container