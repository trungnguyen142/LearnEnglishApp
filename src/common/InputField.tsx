import { View, TouchableOpacity, TextInput, ViewStyle, ImageSourcePropType, Image, StyleSheet } from 'react-native'
import React, { memo } from 'react'
import FlexRow from './FlexRow'
import { actuatedNormalize } from './Metrics'
import SpaceBetween from './SpaceBetween'
import Icon from 'react-native-vector-icons/Ionicons'
import CardView from 'react-native-cardview'

interface Props {
    onChangeText: (text: string) => void
    onPress?: () => void
    inputStyle?: any
    defaultValue?: any
    placeholder: string
    placeholderTextColor?: string
    leftIcon?: ImageSourcePropType
    isLeftIcon?: boolean
    rightIcon?: ImageSourcePropType
    isRightIcon?: boolean
    containerStyle?: ViewStyle
    secureTextEntry?: boolean
    onFocus?: () => void

}

const InputField = (props: Props) => {
    const { onChangeText, onPress, onFocus, inputStyle, defaultValue, placeholder, secureTextEntry, placeholderTextColor, containerStyle, leftIcon, isLeftIcon, isRightIcon, rightIcon } = props
    return (
        <CardView
            cardElevation={1}
            cardMaxElevation={1}
            cornerRadius={5}
            style={[styles.container, containerStyle]}>
            <FlexRow style={{ paddingLeft: actuatedNormalize(16) }}>
                {
                    isLeftIcon ?
                        <Image
                            source={leftIcon}
                            resizeMode='contain'
                            style={{ width: actuatedNormalize(24), height: actuatedNormalize(24) }}
                        /> : null
                }
                <TextInput
                    onChangeText={onChangeText}
                    defaultValue={defaultValue}
                    secureTextEntry={secureTextEntry}
                    style={[styles.input, inputStyle]}
                    placeholder={placeholder}
                    placeholderTextColor={placeholderTextColor}
                    onFocus={onFocus}

                />
            </FlexRow>

            {
                isRightIcon ?
                    <TouchableOpacity
                        onPress={onPress}
                        style={{
                            marginRight: actuatedNormalize(16)
                        }}
                    >
                        {
                            secureTextEntry ?
                                <Icon name='eye-off' size={23} color='#292D32' />
                                :
                                <Icon name='eye' size={23} color='#292D32' />
                        }

                    </TouchableOpacity>
                    : null
            }

        </CardView>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFFFFF',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '100%',
        height: actuatedNormalize(48),
        borderRadius: actuatedNormalize(40),
    },

    input: {
        paddingLeft: actuatedNormalize(8),
        width: '75%'
    },

})

export default memo(InputField)