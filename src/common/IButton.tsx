import { TouchableOpacity, StyleSheet, Image, ImageStyle, ImageSourcePropType } from 'react-native'
import React, { JSXElementConstructor, ReactElement } from 'react'
import { actuatedNormalize } from './Metrics'
import FlexRow from './FlexRow'

interface ButtonProps {
    onPress: () => void
    source?: any
    buttonStyle?: any
    imageStyle?: ImageStyle
    children?: ReactElement | ReactElement[]
    isImage?: boolean
    direction?: "row" | "column" | "row-reverse" | "column-reverse" | undefined
}

const IButton = (props: ButtonProps) => {
    const { onPress, source, buttonStyle, imageStyle, children, isImage, direction } = props
    return (
        <TouchableOpacity
            onPress={onPress}
            activeOpacity={0.5}
            style={buttonStyle}
        >
            <FlexRow style={{ flexDirection: direction }}>
                {isImage && <Image source={source} style={[styles.img, imageStyle]} />}
                {children}
            </FlexRow>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    img: {
        width: actuatedNormalize(30),
        height: actuatedNormalize(30),
        resizeMode: 'contain'
    },
})

export default IButton