import React, { memo } from 'react'
import { View } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder'
import { actuatedNormalize } from "./Metrics";

export function LoadingCard() {
    return (
        <ShimmerPlaceHolder
            style={{ width: '30%', height: 70, borderRadius: 10 }}
            LinearGradient={LinearGradient}
        />
    )
}

