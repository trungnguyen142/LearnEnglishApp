import React from "react"
import { Text } from "react-native"
import MaskedView from "@react-native-community/masked-view"
import LinearGradient from "react-native-linear-gradient"
import { Typography } from "./FontStyles";

const GradientText = (props: any) => {
    return (
        <MaskedView maskElement={<Text {...props} />}>
            <LinearGradient
                colors={[props.color1, props.color2]}
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
            >
                <Text {...props} style={[props.style, { opacity: 0, ...Typography.h3 }]} />
            </LinearGradient>
        </MaskedView>
    );
};

export default GradientText