

const Colors = {
    GREY: '#A1A1A1',
    BLUE: '#3C97FF',
    BLUE_2: '#3089EF',
    BLUE_4: '#0052D4',
    BLACK: '#2A3447',
    WHITE: '#FFFFFF',
    WHITE_2: '#F3FAFF',
    ORANGE: '#EF924C',


}

export default Colors
