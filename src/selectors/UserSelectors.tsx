import { setClientToken } from '../controllers/Client';

export default (state: { user: { data: { token: string | null } } }) => {
    state?.user?.data && setClientToken(state?.user.data?.token)
    return state.user
};
