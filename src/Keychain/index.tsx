import * as Keychain from 'react-native-keychain';

export const setLoginCredentials = async (field1: string, field2: string) => {
    try {
        const { password } = await Keychain.setGenericPassword(field1, field2);

        console.log('setLoginCredentials: ', password);

        return { status: true, access_token: password };
    } catch (e) {
        console.log('keychain access failed ', e);
        return { status: false, error: e };
    }
};

export const getLoginCredentials = async () => {
    try {
        const { password } = await Keychain.getGenericPassword();

        console.log('getLoginCredentials: ', password);

        if (password) {
            return { access_token: password };
        }
        return false;
    } catch (e) {
        console.log('Cannot retrieve keychain data', e);
        return false;
    }
};

export const resetLoginCredentials = async () => {
    try {
        const reset = await Keychain.resetGenericPassword();
        return reset;
    } catch (e) {
        console.log('cannot access or reset keychain data ', e);
        return false;
    }
};
