import React from 'react'
import { Image, Platform } from 'react-native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createStackNavigator } from '@react-navigation/stack'
import { NavigationConstants } from './NavigationContants'
import { actuatedNormalize } from '@/common/Metrics'
import { Typography } from '@/common/FontStyles'
import Colors from '@/common/Colors'
import iconForTab from './TabNavigation'
import VText from '@/common/VText'
import Images from '@/assets/images/IconForTab/index'
import {
    OnBoarding,
    HomePage,
    TestPage,
    PersonalPage,
    ForgotPassword,
    VerifyOTP,
    UpdateProfile,

} from '@/components/index'
import { shallowEqual, useSelector } from 'react-redux'


const Stack = createStackNavigator()
const Tab = createBottomTabNavigator()

function TabIcon(props: any) {
    return (
        <Image style={{
            width: actuatedNormalize(36),
            height: actuatedNormalize(36),
        }}
            source={iconForTab(props)} />
    )
}

const MainTab = () => {
    return (
        <Tab.Navigator
            screenOptions={({ route: { name } }) => ({
                tabBarIcon: ({ ...props }) => {
                    return <TabIcon props={props} name={name} />
                },
            })}
            tabBarOptions={{
                style: {
                    height: Platform.OS === 'ios' ? 90 : 70,
                    alignItems: 'center',
                    justifyContent: 'center',
                    flexDirection: 'column',
                    backgroundColor: '#fff',
                    borderTopWidth: 0.5,
                    paddingTop: actuatedNormalize(10),
                    paddingBottom: Platform.OS === 'ios' ? 25 : 10,
                    shadowColor: '#000',
                    shadowOffset: {
                        width: 1,
                        height: 6,
                    },
                    shadowOpacity: 0.5,
                    shadowRadius: 3.84,
                    elevation: 10,
                },
            }}
        >
            <Tab.Screen
                name={NavigationConstants.Home}
                component={HomePage}
                options={{
                    tabBarLabel: ({ focused, color }) => (
                        <VText textStyle={{ color: focused ? '#0052D4' : color }}>Trang chủ</VText>
                    ),
                }}
            />
            <Tab.Screen
                name={NavigationConstants.Test}
                component={TestPage}
                options={{
                    tabBarLabel: ({ focused, color }) => (
                        <VText textStyle={{ color: focused ? '#6D62F4' : color }}>Kiểm tra</VText>
                    ),
                }}
            />
            <Tab.Screen
                name={NavigationConstants.Personal}
                component={PersonalPage}
                options={{
                    tabBarLabel: ({ focused, color }) => (
                        <VText textStyle={{ color: focused ? '#EC6665' : color }}>Cá nhân</VText>
                    ),
                }}
            />
        </Tab.Navigator>


    )
}

const AppNavigator = () => {
    const checkIsGoHome = useSelector((state: any) => state.user.isGoHome, shallowEqual)
    return (
        <Stack.Navigator
            initialRouteName={checkIsGoHome ? NavigationConstants.Main : NavigationConstants.OnBoarding}
        >
            <Stack.Screen
                name={NavigationConstants.OnBoarding}
                component={OnBoarding}
                options={{
                    headerShown: false,
                }}
            />

            <Stack.Screen
                name={NavigationConstants.ForgotPassword}
                component={ForgotPassword}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name={NavigationConstants.VerifyOTP}
                component={VerifyOTP}
                options={{
                    headerShown: false,
                }}
            />

            <Stack.Screen
                name={NavigationConstants.Main}
                component={MainTab}
                options={{
                    headerShown: false,
                }}
            />

            <Stack.Screen
                name={NavigationConstants.UpdateProfile}
                component={UpdateProfile}
                options={{
                    headerShown: false,
                }}
            />

        </Stack.Navigator>
    )
}

export default AppNavigator
