

export const NavigationConstants = {
    OnBoarding: 'OnBoarding',
    Home: 'Home',
    Test: 'Test',
    Personal: 'Personal',
    Main: 'Main',
    ForgotPassword: 'ForgotPassword',
    VerifyOTP: 'VerifyOTP',
    UpdateProfile: 'UpdateProfile',


}