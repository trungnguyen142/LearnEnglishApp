import { NavigationConstants } from './NavigationContants'
import Images from '@/assets/images/IconForTab/index'

const iconForTab = (props: any) => {
    const focused = props.props.focused
    switch (props.name) {
        case NavigationConstants.Home:
            return focused ? Images.Home : Images.Home_Non_Active
        case NavigationConstants.Test:
            return focused ? Images.Test : Images.Test_Non_Active
        case NavigationConstants.Personal:
            return focused ? Images.Personal : Images.Personal_Non_Active
        default:
            return null
    }
}

export default iconForTab