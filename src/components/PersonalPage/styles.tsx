import { actuatedNormalize } from '@/common/Metrics'
import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    container: {
        width: actuatedNormalize(178),
        height: actuatedNormalize(178),
        borderRadius: 200,
        backgroundColor: '#68B7FF',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
    },

    borderImage: {
        backgroundColor: 'rgba(60, 130, 183, 0.1)',
        width: actuatedNormalize(150),
        height: actuatedNormalize(150),
        borderRadius: 200,
        alignItems: 'center',
        justifyContent: 'center'
    },

    chooseImg: {
        backgroundColor: 'white',
        position: 'absolute',
        padding: 8,
        borderRadius: 50,
        bottom: 20,
        right: 15,
    },

    linearView: {
        width: 184,
        height: 40,
        borderRadius: 10,
        marginVertical: 12,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    },

    editName: {
        backgroundColor: 'white',
        position: 'absolute',
        padding: 6,
        borderRadius: 50,
        bottom: 15,
        right: -20,
    },

    tabView: {
        width: actuatedNormalize(104),
        height: actuatedNormalize(40),
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 24,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
    },

    btnGo: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'white',
        height: actuatedNormalize(48),
        marginHorizontal: 16,
        borderRadius: 10,
        paddingLeft: 16,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
    },

    btnLogout: {
        width: actuatedNormalize(120),
        height: actuatedNormalize(48),
        borderRadius: 40,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        marginTop: actuatedNormalize(40)
    },
})

export default styles