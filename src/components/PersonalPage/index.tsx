import { View, SafeAreaView, Image, TouchableOpacity, ScrollView } from 'react-native'
import React, { useState } from 'react'
import LinearGradient from 'react-native-linear-gradient'
import { actuatedNormalize } from '@/common/Metrics'
import HomeIcon from '@/assets/images/HomeIcon'
import IconUpdateProfile from '@/assets/images/IconUpdateProfile'
import VText from '@/common/VText'
import FlexRow from '@/common/FlexRow'
import Button from '@/common/Button'
import styles from './styles'

const tabView = [
    {
        id: 1,
        title: 'Thông tin',
    },
    {
        id: 2,
        title: 'Thành tích',
    },
    {
        id: 3,
        title: 'Bạn bè',
    },
]

export const PersonalPage = () => {
    const [tabIndex, setTabIndex] = useState(0)
    return (
        <LinearGradient
            colors={['#4DA1FE', '#1FC7FB']}
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            style={{ flex: 1 }}
        >
            <SafeAreaView>
                <View
                    style={styles.container}
                >
                    <View
                        style={styles.borderImage}
                    >
                        <Image source={HomeIcon.test} style={{
                            width: actuatedNormalize(110),
                            height: actuatedNormalize(110),
                            borderRadius: 200,
                        }} />
                        <TouchableOpacity
                            style={styles.chooseImg}>
                            <Image source={IconUpdateProfile.camera_blue}
                                style={{
                                    width: 16,
                                    height: 16,
                                }} />
                        </TouchableOpacity>
                    </View>
                </View>
                <FlexRow style={{ alignSelf: 'center' }}>
                    <LinearGradient
                        colors={['#23DEF3', '#0DC4F4']}
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 0 }}
                        style={styles.linearView}
                    >
                        <VText numberOfLines={1} fontWeight='700' textStyle={{ color: 'white' }}>
                            Lưu Chí Trung Thành
                        </VText>
                    </LinearGradient>
                    <TouchableOpacity
                        style={styles.editName}>
                        <Image source={IconUpdateProfile.edit_blue}
                            style={{
                                width: 16,
                                height: 16,
                            }} />
                    </TouchableOpacity>
                </FlexRow>
            </SafeAreaView>
            <View style={{ flex: 1, backgroundColor: '#F3FAFF', marginTop: 42 }}>
                <FlexRow style={{ position: 'absolute', top: -20 }} >
                    {
                        tabView.map((item, index) => {
                            let actived = false
                            if (tabIndex === index) {
                                actived = true
                            }
                            return (
                                <Button
                                    key={`-item${item.id}`}
                                    text={item.title}
                                    onPress={() => setTabIndex(index)}
                                    buttonStyle={[styles.tabView, { backgroundColor: actived ? '#4DA1FE' : '#F3FAFF', }]}
                                    textStyle={{
                                        color: actived ? 'white' : '#89A6C5'
                                    }}
                                />
                            )
                        })
                    }
                </FlexRow>
                <ScrollView style={{ marginTop: 40 }}>
                    <TouchableOpacity
                        activeOpacity={0.5}
                        style={styles.btnGo}>
                        <Image source={IconUpdateProfile.shield}
                            style={{
                                width: actuatedNormalize(24),
                                height: actuatedNormalize(24),
                            }} />
                        <VText textStyle={{
                            paddingLeft: 8
                        }}>
                            Đổi mật khẩu
                        </VText>
                    </TouchableOpacity>

                    <TouchableOpacity
                        activeOpacity={0.5}
                        style={[styles.btnGo, { marginTop: 16 }]}>
                        <Image
                            source={IconUpdateProfile.call_add}
                            style={{
                                width: actuatedNormalize(24),
                                height: actuatedNormalize(24),
                            }} />
                        <VText textStyle={{
                            paddingLeft: 8
                        }}>
                            Thêm số điện thoại
                        </VText>
                    </TouchableOpacity>

                    <TouchableOpacity
                        activeOpacity={0.5}
                        style={styles.btnLogout}
                    >
                        <VText textStyle={{
                            color: '#89A6C5'
                        }} >
                            Đăng xuất
                        </VText>
                    </TouchableOpacity>
                </ScrollView>
            </View>

        </LinearGradient>

    )
}

