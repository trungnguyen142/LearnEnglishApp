import { View, Text, SafeAreaView, TouchableOpacity } from 'react-native'
import React, { useState } from 'react'
import LinearGradient from 'react-native-linear-gradient'
import Icon from 'react-native-vector-icons/Ionicons'
import Colors from '@/common/Colors'
import VText from '@/common/VText'
import SpaceBetween from '@/common/SpaceBetween'
import InputField from '@/common/InputField'
import IconLogin from '@/assets/images/InputIcon'
import LButton from '@/common/LButton'
import BgButton from '@/assets/images/BgButton'
import { NavigationConstants } from '../Navigation/NavigationContants'

export const ForgotPassword = ({ navigation }: { navigation: any }) => {
    const [email, setEmail] = useState("")

    const handleGoOTP = () => {
        navigation.navigate(NavigationConstants.VerifyOTP)
    }

    return (
        <LinearGradient
            colors={['#4DA1FE', '#1FC7FB']}
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            style={{ flex: 1 }}
        >
            <SafeAreaView style={{ paddingHorizontal: 16 }}>
                <SpaceBetween>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Icon name='chevron-back' size={28} color={Colors.WHITE} />
                    </TouchableOpacity>
                    <VText textStyle={{
                        color: Colors.WHITE,
                        fontWeight: '700',
                        fontSize: 24
                    }}>
                        Quên mật khẩu
                    </VText>
                    <View style={{ width: 30 }} />
                </SpaceBetween>
            </SafeAreaView>
            <View style={{
                flex: 1,
                backgroundColor: Colors.WHITE_2,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
                marginTop: 20,
                padding: 16
            }}>
                <VText>
                    Bạn vui lòng nhập email để khôi phục mật khẩu
                </VText>
                <InputField
                    onChangeText={(text: string) => setEmail(text)}
                    placeholder='Email'
                    placeholderTextColor='#89A6C5'
                    containerStyle={{ marginTop: 16 }}
                    isLeftIcon={true}
                    leftIcon={email.length ? IconLogin.Account_blue : IconLogin.Account}
                />

                <LButton
                    onPress={() => handleGoOTP()}
                    text='Tiếp theo'
                    buttonStyle={{
                        marginTop: 90
                    }}
                    source={BgButton.bg_button_blue}
                />
            </View>
        </LinearGradient>
    )
}
