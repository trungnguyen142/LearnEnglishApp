import { ImageBackground, StyleSheet, Text, Image, TouchableOpacity, View } from 'react-native'
import React, { memo, useState } from 'react'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import styles from './styles'
import VText from '@/common/VText'
import Button from '@/common/Button'
import FlexRow from '@/common/FlexRow'
import InputField from '@/common/InputField'
import BgButton from '@/assets/images/BgButton'
import RouteLogin from '@/assets/images/IconRouteLogin'
import IconLogin from '@/assets/images/InputIcon'
import { NavigationConstants } from '../Navigation/NavigationContants'
import { Go_to_home, userLogin } from '@/actions/UserActions'
import { useDispatch } from 'react-redux'
import { WToastShowError } from '@/common/WToast'
import { useNavigation } from '@react-navigation/native'
const listRouteLogin: any = [
    {
        id: 1,
        img: RouteLogin.facebook,
    },
    {
        id: 2,
        img: RouteLogin.apple,
    },
    {
        id: 3,
        img: RouteLogin.google,
    },
]

const Signin = () => {
    const navigation = useNavigation()
    const dispatch = useDispatch()
    const [warningInput, setWarningInput] = useState(false)
    const [show, setShow] = useState(false)
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")

    const handleLogin = () => {
        if (!password.length || !email.length) {
            setWarningInput(true)
        }
        else {
            userLogin(email, password)(dispatch)
                .then(res => {
                    console.log('res', res);

                })
                .catch(err => {
                    WToastShowError('Đăng nhập thất bại,vui lòng thử lại!')
                })
        }
    }

    return (
        <KeyboardAwareScrollView>
            <InputField
                onChangeText={(text: string) => setEmail(text)}
                placeholder='Email'
                placeholderTextColor='#89A6C5'
                containerStyle={{ marginTop: 16 }}
                isLeftIcon={true}
                leftIcon={email.length ? IconLogin.Account_blue : IconLogin.Account}
                onFocus={() => setWarningInput(false)}
            />
            {
                warningInput && !email.length &&
                <VText textStyle={{ color: 'red', paddingTop: 6 }}>
                    Không được để trống
                </VText>
            }
            <InputField
                onChangeText={(text: string) => setPassword(text)}
                placeholder='Mật khẩu'
                placeholderTextColor='#89A6C5'
                secureTextEntry={!show}
                isLeftIcon={true}
                isRightIcon={true}
                containerStyle={{ marginVertical: 16 }}
                leftIcon={password.length ? IconLogin.Password_blue : IconLogin.Password}
                onPress={() => setShow(!show)}
                onFocus={() => setWarningInput(false)}
            />
            {
                warningInput && !password.length &&
                <VText textStyle={{ color: 'red' }}>
                    Không được để trống
                </VText>
            }
            <Button
                onPress={() => navigation.navigate(NavigationConstants.ForgotPassword)}
                text='Quên mật khẩu ?'
                buttonStyle={{ alignSelf: 'flex-end' }}
                textStyle={styles.txtTab}
                isGradient={false}
            />

            <TouchableOpacity
                activeOpacity={0.5}
                onPress={handleLogin}
            >
                <ImageBackground
                    style={styles.btnSubmit}
                    resizeMode='contain'
                    source={BgButton.bg_button_blue}>
                    <Text style={styles.txtLogin}>
                        Đăng nhập
                    </Text>
                </ImageBackground>
            </TouchableOpacity>
            <View style={{
                alignSelf: 'center',
                alignItems: 'center',
            }}>
                <VText>
                    Đăng nhập nhanh qua
                </VText>

            </View>
            <FlexRow style={{ alignSelf: 'center' }}>
                {
                    listRouteLogin.map((item: any) => {
                        return (
                            <TouchableOpacity
                                key={item?.id}
                                activeOpacity={0.5}
                                style={{ margin: 8 }}
                            >
                                <Image
                                    source={item?.img}
                                    style={{
                                        width: 48,
                                        height: 48,
                                    }}
                                />
                            </TouchableOpacity>
                        )
                    })
                }
            </FlexRow>
            <TouchableOpacity
                onPress={() => {
                    dispatch(Go_to_home())
                    navigation.navigate(NavigationConstants.Main)
                }}
            >
                <VText textStyle={{ color: '#EF924C', textAlign: 'center' }}>
                    Để sau
                </VText>
            </TouchableOpacity>
        </KeyboardAwareScrollView>
    )
}

export default memo(Signin)