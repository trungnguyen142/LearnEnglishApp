import { View, Text, SafeAreaView, TouchableOpacity, Image, ImageBackground, KeyboardAvoidingView } from 'react-native'
import React, { useState } from 'react'
import Swiper from 'react-native-web-swiper'
import Images from '@/assets/images/OnboardingSliders'
import LinearGradient from 'react-native-linear-gradient'
import styles from './styles'
import FlexRow from '@/common/FlexRow'
import Button from '@/common/Button'
import Signin from './Sign-in'
import Signup from './Sign-up'

interface Swipers {
    id: number,
    img: any,
    title: string,
}


const swiperItems: Swipers[] = [
    {
        id: 1,
        img: Images.slider1,
        title: 'HỌC TIẾNG ANH THẬT DỄ',
    },
    {
        id: 2,
        img: Images.slider2,
        title: 'PHƯƠNG PHÁP HIỆU QUẢ',
    },
    {
        id: 3,
        img: Images.slider3,
        title: 'PHÙ HỢP LỨA TUỔI CỦA BÉ',
    },
]



export const OnBoarding = ({ navigation }: { navigation: any }) => {
    const [currentIndex, setCurentIndex] = useState(0)
    const [tabIndex, setTabIndex] = useState(0)

    const mapLinear: any = {
        0: ['#4DA1FE', '#1FC7FB'],
        1: ['#FBB94A', '#FDA459'],
        2: ['#8EE048', '#6ACF5A'],
    }

    return (
        <LinearGradient
            colors={mapLinear[currentIndex]}
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            style={styles.container}
        >
            <SafeAreaView style={{ width: '100%', height: '41%' }}>
                <Swiper
                    loop={true}
                    timeout={3}
                    onIndexChanged={(index: number) => setCurentIndex(index)}
                    controlsProps={{
                        prevPos: false,
                        nextPos: false,
                        DotComponent: ({ index, isActive, onPress }) => {
                            if (isActive) {
                                return (
                                    <View style={[styles.innerDot, { width: isActive ? 30 : 10 }]} />
                                )
                            }
                            else {
                                return (
                                    <View style={styles.dotNoActive} />
                                )
                            }
                        }
                    }}
                >
                    {
                        swiperItems.map(item => {
                            return (
                                <View
                                    style={styles.itemCenter}
                                    key={item?.id}>
                                    <Text style={styles.textTitle}>
                                        {item?.title}
                                    </Text>
                                    <Image style={styles.imgSlide}
                                        source={item?.img} />
                                </View>
                            )
                        })
                    }
                </Swiper>
            </SafeAreaView>
            <View style={styles.containerLogin}>
                <FlexRow>
                    <View style={styles.btnTab}>
                        <Button
                            onPress={() => {
                                setTabIndex(0)
                            }}
                            color1='#4DA1FE'
                            color2='#1FC7FB'
                            text='ĐĂNG NHẬP'
                            buttonStyle={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}
                            textStyle={styles.txtTab}
                            isGradient={true}
                        />
                        {
                            tabIndex === 0 ?
                                <LinearGradient
                                    colors={['#4DA1FE', '#1FC7FB']}
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 0 }}
                                    style={{ width: '90%', height: 4, borderRadius: 8, marginTop: 8 }}
                                />
                                : null
                        }
                    </View>
                    <View style={styles.btnTab}>
                        <Button
                            onPress={() => {
                                setTabIndex(1)
                            }}
                            color1='#FDA459'
                            color2='#FBB94A'
                            text='ĐĂNG KÝ'
                            buttonStyle={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}
                            textStyle={styles.txtTab}
                            isGradient={true}
                        />
                        {
                            tabIndex === 1 ?
                                <LinearGradient
                                    colors={['#FDA459', '#FBB94A']}
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 0 }}
                                    style={{ width: '90%', height: 4, borderRadius: 8, marginTop: 8 }}
                                /> : null
                        }
                    </View>
                </FlexRow>
                {
                    tabIndex === 0 ?
                        <Signin />
                        :
                        <Signup />
                }
            </View>
        </LinearGradient >
    )
}
