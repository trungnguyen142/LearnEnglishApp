import { View, Text, TouchableOpacity, ImageBackground } from 'react-native'
import React, { memo, useState } from 'react'
import styles from './styles'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import InputField from '@/common/InputField'
import VText from '@/common/VText'
import IconLogin from '@/assets/images/InputIcon'
import { useNavigation } from '@react-navigation/native'
import BgButton from '@/assets/images/BgButton'
import { NavigationConstants } from '../Navigation/NavigationContants'

const Signup = () => {
    const navigation = useNavigation()
    const [showRePassword, setShowRePassword] = useState(false)
    const [show, setShow] = useState(false)
    const [regisEmail, setRegisEmail] = useState("")
    const [regisPassword, setRegisPassword] = useState("")
    const [regisRePassword, setRegisRePassword] = useState("")

    const handleRegister = () => {
        navigation.navigate(NavigationConstants.VerifyOTP)
    }

    return (
        <KeyboardAwareScrollView>
            <InputField
                onChangeText={(text: string) => setRegisEmail(text)}
                placeholder='Email'
                placeholderTextColor='#89A6C5'
                containerStyle={{ marginTop: 16 }}
                isLeftIcon={true}
                leftIcon={regisEmail.length ? IconLogin.Account_blue : IconLogin.Account}
            />
            <InputField
                onChangeText={(text: string) => setRegisPassword(text)}
                placeholder='Mật khẩu'
                placeholderTextColor='#89A6C5'
                secureTextEntry={!show}
                isLeftIcon={true}
                isRightIcon={true}
                containerStyle={{ marginTop: 16 }}
                leftIcon={regisPassword.length ? IconLogin.Password_blue : IconLogin.Password}
                onPress={() => setShow(!show)}
            />
            <InputField
                onChangeText={(text: string) => setRegisRePassword(text)}
                placeholder='Nhập lại mật khẩu'
                placeholderTextColor='#89A6C5'
                secureTextEntry={!showRePassword}
                isLeftIcon={true}
                isRightIcon={true}
                containerStyle={{ marginVertical: 16 }}
                leftIcon={regisRePassword.length ? IconLogin.Password_blue : IconLogin.Password}
                onPress={() => setShowRePassword(!showRePassword)}
            />

            <TouchableOpacity
                activeOpacity={0.5}
                onPress={handleRegister}
            >
                <ImageBackground
                    style={styles.btnSubmit}
                    resizeMode='contain'
                    source={BgButton.bg_button_blue}>
                    <Text style={styles.txtLogin}>
                        Đăng ký
                    </Text>
                </ImageBackground>
            </TouchableOpacity>
            <TouchableOpacity
                style={{
                    width: 80,
                    height: 40,
                    alignSelf: 'center',
                }}
                onPress={() => navigation.navigate(NavigationConstants.Main)}
            >
                <VText textStyle={{ color: '#EF924C', textAlign: 'center' }}>
                    Để sau
                </VText>
            </TouchableOpacity>
        </KeyboardAwareScrollView>
    )
}

export default memo(Signup)