import { StyleSheet, Platform } from "react-native"
import Colors from '../../common/Colors'
import { Typography } from '../../common/FontStyles'
import { actuatedNormalize } from '../../common/Metrics'

const styles = StyleSheet.create({

    container: {
        flex: 1,

    },

    viewDot: {
        borderWidth: 1,
        borderRadius: 20,
        width: actuatedNormalize(16),
        height: actuatedNormalize(16),
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#E9A25A'
    },
    innerDot: {
        backgroundColor: Colors.WHITE,
        width: actuatedNormalize(6),
        height: actuatedNormalize(6),
        borderRadius: 20,
        margin: actuatedNormalize(10)
    },
    dotNoActive: {
        backgroundColor: Colors.WHITE,
        width: actuatedNormalize(6),
        height: actuatedNormalize(6),
        borderRadius: 10,
        margin: actuatedNormalize(10)
    },

    itemCenter: {
        flex: 1,
        alignItems: 'center',
    },

    textTitle: {
        paddingTop: actuatedNormalize(30),
        ...Typography.h1,
        color: Colors.WHITE,
    },
    imgSlide: {
        resizeMode: 'contain',
        width: '100%',
        height: Platform.OS === 'android' ? actuatedNormalize(180) : actuatedNormalize(215),
    },

    containerLogin: {
        backgroundColor: '#F3FAFF',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        paddingTop: actuatedNormalize(12),
        paddingHorizontal: actuatedNormalize(16),
        height: '100%'

    },

    btnTab: {
        width: '50%',
        alignItems: 'center',
        justifyContent: 'center',
    },

    txtTab: {
        ...Typography.h1,
        fontSize: actuatedNormalize(14)
    },

    btnSubmit: {
        width: actuatedNormalize(200),
        height: actuatedNormalize(48),
        marginTop: actuatedNormalize(18),
        marginBottom: actuatedNormalize(18),
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },

    txtLogin: {
        ...Typography.h1,
        fontSize: actuatedNormalize(14),
        color: Colors.WHITE,
    },
})

export default styles