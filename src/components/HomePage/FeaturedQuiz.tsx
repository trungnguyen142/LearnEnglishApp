import { View, Image } from 'react-native'
import React, { memo } from 'react'
import IButton from '@/common/IButton'
import styles from './styles'
import VText from '@/common/VText'
import FlexRow from '@/common/FlexRow'
import Icon from 'react-native-vector-icons/Ionicons'
import AntDesign from 'react-native-vector-icons/AntDesign'
import CardView from 'react-native-cardview'

interface Props {
    item: any;
    onPress: () => void
}

const FeaturedQuiz = (props: Props) => {
    const { item, onPress } = props
    return (
        <CardView
            cardElevation={1}
            cardMaxElevation={1}
            cornerRadius={5}
            style={styles.cardItem}
        >
            <IButton
                onPress={onPress}
                buttonStyle={{ flex: 1 }}
                direction='column'
            >
                <Image source={item?.image} style={styles.imageItem} />
                <View style={{
                    padding: 8,
                }}>
                    <VText numberOfLines={1} textStyle={{
                        paddingVertical: 16,
                        fontWeight: '700'
                    }}>
                        {item?.title}
                    </VText>
                    <FlexRow>
                        <Icon name='document-text' size={18} />
                        <VText textStyle={{ paddingLeft: 4 }}>
                            {item?.socauhoi}
                        </VText>
                    </FlexRow>
                    <FlexRow style={{
                        marginTop: 4
                    }}>
                        <AntDesign name='clockcircle' size={18} />
                        <VText textStyle={{ paddingLeft: 4 }}>
                            {item?.thoigian}
                        </VText>
                    </FlexRow>
                </View>
            </IButton>
        </CardView>
    )
}

export default memo(FeaturedQuiz)