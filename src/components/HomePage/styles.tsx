import { StyleSheet } from 'react-native'
import Colors from '@/common/Colors'
import { actuatedNormalize, PADDING_BOTTOM } from '@/common/Metrics'



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.WHITE
    },

    btnPickClass: {
        width: actuatedNormalize(64),
        height: actuatedNormalize(32),
        borderRadius: actuatedNormalize(20),
    },

    txtPickClass: {
        color: Colors.WHITE,
        fontWeight: '500',
    },

    body: {
        flex: 1,
        backgroundColor: Colors.WHITE_2,
        padding: actuatedNormalize(16),
    },

    avatar: {
        width: actuatedNormalize(48),
        height: actuatedNormalize(48),
        borderRadius: 50,
        borderWidth: 1,
    },

    cardBG: {
        width: '100%',
        height: actuatedNormalize(170),
    },

    cirkleView: {
        width: actuatedNormalize(68),
        height: actuatedNormalize(68),
        borderRadius: 68,
        backgroundColor: Colors.WHITE,
        alignItems: 'center',
        justifyContent: 'center'
    },

    innerCirkle: {
        width: actuatedNormalize(40),
        height: actuatedNormalize(40),
    },

    notifiImage: {
        width: actuatedNormalize(30),
        height: actuatedNormalize(30),

    },

    cardNotifi: {
        width: actuatedNormalize(32),
        height: actuatedNormalize(32),
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
    },

    cardClass: {
        alignItems: 'center',
        justifyContent: 'center',
        width: actuatedNormalize(64),
        height: actuatedNormalize(32),
        borderRadius: actuatedNormalize(20),
    },

    cardNote: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        width: actuatedNormalize(64),
        height: actuatedNormalize(32),
        borderRadius: 20,
        marginLeft: actuatedNormalize(12),
    },

    cardItem: {
        borderRadius: 8,
        width: actuatedNormalize(160),
        marginRight: actuatedNormalize(16),
        backgroundColor: 'white',

    },

    imageItem: {
        width: '100%',
        height: actuatedNormalize(144),
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
    },

    rightDown: {
        width: actuatedNormalize(100),
        height: actuatedNormalize(40),
        borderRadius: 40,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
    },



})

export default styles