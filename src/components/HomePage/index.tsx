import { View, SafeAreaView, ScrollView, Image, RefreshControl, FlatList } from 'react-native'
import React, { useState, useRef, useCallback, useEffect } from 'react'
import styles from './styles'
import VText from '@/common/VText'
import FlexRow from '@/common/FlexRow'
import Images from '@/assets/images/HomeIcon'
import SpaceBetween from '@/common/SpaceBetween'
import { fontWeight } from '@/common/Metrics'
import { useSelector, useDispatch } from 'react-redux'
import { LoadingCard } from '@/common/LoadingSkeleton'
import { useScrollToTop } from '@react-navigation/native'
import { NavigationConstants } from '../Navigation/NavigationContants'
import FeaturedQuiz from './FeaturedQuiz'
import CardPressed from '@/common/CardPressed'
import Header from '@/common/Header'
import LButton from '@/common/LButton'
import GradientText from '@/common/GradientText'

const listFake = [
    {
        id: 1,
        image: Images.test,
        title: 'Test 1: Abc word',
        socauhoi: '10 câu hỏi',
        thoigian: '20 phút',
    },
    {
        id: 2,
        image: Images.test,
        title: 'Test 1: Abc word',
        socauhoi: '10 câu hỏi',
        thoigian: '20 phút',
    },
    {
        id: 3,
        image: Images.test,
        title: 'Test 1: Abc word',
        socauhoi: '10 câu hỏi',
        thoigian: '20 phút',
    },
]

export const HomePage = ({ navigation }: { navigation: any }) => {
    const userData = useSelector(state => state.user?.data)
    const [refreshing, setRefreshing] = useState(false)
    const scrollRef = useRef(null)
    useScrollToTop(scrollRef)
    const handleGoDetail = () => {
        navigation.navigate(NavigationConstants.OnBoarding)
    }

    const wait = (timeout: number | undefined) => {
        return new Promise(resolve => setTimeout(resolve, timeout))
    }

    const onRefresh = useCallback(() => {
        setRefreshing(true)
        wait(1000).then(() => {
            setRefreshing(false)
        })

    }, [])

    // useEffect(() => {
    //     const unsubscribe = navigation.addListener('tabPress', () => {
    //         onRefresh()
    //     })
    //     return unsubscribe
    // }, [navigation])

    return (
        <SafeAreaView style={styles.container}>
            <Header />

            <ScrollView
                style={styles.body}
                ref={scrollRef}
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={onRefresh}
                    />
                }
            >
                {
                    userData &&
                    <FlexRow>
                        <Image source={Images.star} style={styles.avatar} />
                        <View style={{
                            paddingLeft: 12
                        }}>
                            <VText>
                                Chào mừng bạn
                            </VText>
                            <VText fontWeight='700'>
                                Nguyễn Tuấn Trung
                            </VText>
                        </View>
                    </FlexRow>
                }
                <LButton
                    onPress={() => null}
                    source={Images.card_blue}
                    isChildren={true}
                    style={styles.cardBG}
                    imageStyle={{ borderRadius: 10 }}
                >
                    <SpaceBetween style={{ width: '90%' }}>
                        <View style={styles.cirkleView}>
                            <Image source={Images.ontap_kienthuc} style={styles.innerCirkle} />
                        </View>
                        <VText textStyle={{ width: 130, color: '#0052D4' }}>
                            Ôn tập kiến thức trên trường học bằng thẻ ghi nhớ
                        </VText>
                    </SpaceBetween>
                    <SpaceBetween style={{ width: '90%', marginTop: 12 }}>
                        <VText textStyle={{ width: 84, color: 'white', fontWeight: fontWeight.SemiBold }}>
                            Ôn tập kiến thức
                        </VText>
                        <View style={styles.rightDown}>
                            <GradientText
                                color1='#0052D4'
                                color2='#3C97FF'
                                style={{ fontWeight: fontWeight.SemiBold }}>
                                Bắt đầu
                            </GradientText>
                        </View>
                    </SpaceBetween>
                </LButton>

                <LButton
                    onPress={() => null}
                    source={Images.card_yellow}
                    isChildren={true}
                    style={[styles.cardBG, { marginTop: 0 }]}
                    imageStyle={{ borderRadius: 10 }}
                >
                    <SpaceBetween style={{ width: '90%' }}>
                        <View style={styles.cirkleView}>
                            <Image source={Images.luyentap} style={styles.innerCirkle} />
                        </View>
                        <VText textStyle={{ width: 150, color: '#EC6665' }}>
                            Luyện tập kiến thức đã học bằng các bài kiểm tra thú vị
                        </VText>
                    </SpaceBetween>
                    <SpaceBetween style={{ width: '90%', marginTop: 12 }}>
                        <VText textStyle={{ width: 110, color: 'white', fontWeight: fontWeight.SemiBold }}>
                            Luyện tập với bài kiểm tra
                        </VText>
                        <View style={styles.rightDown}>
                            <GradientText
                                color1='#FDA459'
                                color2='#FBB94A'
                                style={{ fontWeight: fontWeight.SemiBold }}>
                                Bắt đầu
                            </GradientText>
                        </View>
                    </SpaceBetween>
                </LButton>
                <VText textStyle={{
                    fontWeight: fontWeight.SemiBold,
                    fontSize: 16
                }}>
                    Bài kiểm tra nổi bật
                </VText>
                <FlatList
                    data={listFake}
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    contentContainerStyle={{ marginVertical: 16 }}
                    keyExtractor={(item, index) => item?.id.toString()}
                    renderItem={({ item, index }) => <FeaturedQuiz item={item} onPress={handleGoDetail} />}
                />

                <VText textStyle={{
                    fontWeight: fontWeight.SemiBold,
                    fontSize: 16,
                    marginBottom: 16
                }}>
                    Thẻ ghi nhớ hôm nay
                </VText>
                <CardPressed />
            </ScrollView>
        </SafeAreaView >
    )
}

