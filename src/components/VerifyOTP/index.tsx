import { View, Text, SafeAreaView, TouchableOpacity, StyleSheet, ImageBackground } from 'react-native'
import React, { useState } from 'react'
import { actuatedNormalize } from '@/common/Metrics'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Typography } from '@/common/FontStyles'
import LinearGradient from 'react-native-linear-gradient'
import Icon from 'react-native-vector-icons/Ionicons'
import Colors from '@/common/Colors'
import VText from '@/common/VText'
import SpaceBetween from '@/common/SpaceBetween'
import {
    CodeField,
    Cursor,
    useBlurOnFulfill,
    useClearByFocusCell,
} from 'react-native-confirmation-code-field'
import CountDown from 'react-native-countdown-component'
import moment from 'moment'
import BgButton from '@/assets/images/BgButton'
import { NavigationConstants } from '../Navigation/NavigationContants'

export const VerifyOTP = ({ navigation }: { navigation: any }) => {
    const CELL_COUNT = 6
    const [value, setValue] = useState('')
    const [expire_time, setExpireTime] = useState(moment(new Date()).add(2, 'minutes'))
    const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT })
    const [props, getCellOnLayoutHandler] = useClearByFocusCell({
        value,
        setValue,
    })

    const renderCountDown = () => {
        let countdown = moment(moment(new Date()).add(2, 'minutes'), 'YYYY-MM-DD HH:mm:ss').unix() - moment().unix()
        if (expire_time && countdown > 0) {
            return (
                <View style={styles.otpContainer}>
                    <VText textStyle={styles.txtSeparator}>Gửi lại OTP:</VText>
                    <CountDown
                        until={countdown}
                        size={14}
                        onFinish={() => setExpireTime(null)}
                        digitStyle={{ marginHorizontal: 0 }}
                        separatorStyle={styles.txtSeparator}
                        digitTxtStyle={styles.txtSeparator}
                        timeToShow={['M', 'S']}
                        timeLabels={{ m: null, s: null }}
                        showSeparator
                    />
                </View>
            )
        } else
            return (
                <TouchableOpacity
                    style={styles.btnResendOtp}
                    onPress={resendOtp}>
                    <VText>{"Gửi lại mã OTP"}</VText>
                </TouchableOpacity>
            )
    }

    const resendOtp = () => {
        setExpireTime(moment(new Date()).add(2, 'minutes'))

    }

    const onVerifyOTP = () => {
        navigation.navigate(NavigationConstants.UpdateProfile)
    }

    return (
        <LinearGradient
            colors={['#4DA1FE', '#1FC7FB']}
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            style={{ flex: 1 }}
        >
            <SafeAreaView style={{ paddingHorizontal: 16 }}>
                <SpaceBetween>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Icon name='chevron-back' size={28} color={Colors.WHITE} />
                    </TouchableOpacity>
                    <VText fontWeight='700' textStyle={{
                        color: Colors.WHITE,
                        fontSize: 24
                    }}>
                        Xác thực OTP
                    </VText>
                    <View style={{ width: 30 }} />
                </SpaceBetween>
            </SafeAreaView>
            <View style={{
                flex: 1,
                backgroundColor: Colors.WHITE_2,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
                marginTop: 20,
                padding: 16
            }}>
                <KeyboardAwareScrollView>
                    <VText>
                        Mã xác nhận gồm 6 ký tự sẽ được gửi qua email. Bạn vui lòng đợi trong giây lát
                    </VText>
                    <VText textStyle={{
                        marginVertical: 12,
                        fontSize: 18,
                        fontWeight: '700',
                    }}>
                        Mã OTP
                    </VText>
                    <CodeField
                        ref={ref}
                        {...props}
                        // Use `caretHidden={false}` when users can't paste a text value, because context menu doesn't appear
                        value={value}
                        onChangeText={setValue}
                        cellCount={CELL_COUNT}
                        rootStyle={styles.codeFieldRoot}
                        keyboardType="number-pad"
                        textContentType="oneTimeCode"
                        renderCell={({ index, symbol, isFocused }) => (
                            <VText
                                key={index}
                                textStyle={[styles.cell, isFocused && styles.focusCell]}
                                onLayout={getCellOnLayoutHandler(index)}>
                                {symbol || (isFocused ? <Cursor /> : null)}
                            </VText>
                        )}


                    />
                    <SpaceBetween style={{
                        paddingVertical: 16
                    }}>
                        <VText textStyle={{
                            color: '#48A7FE'
                        }}>
                            Đổi số điện thoại/Email
                        </VText>
                        {renderCountDown()}
                    </SpaceBetween>

                    <TouchableOpacity
                        activeOpacity={0.5}
                        onPress={onVerifyOTP}
                    >
                        <ImageBackground
                            style={styles.btnSubmit}
                            resizeMode='contain'
                            source={BgButton.bg_button_blue}>
                            <Text style={styles.txtLogin}>
                                Xác nhận
                            </Text>
                        </ImageBackground>
                    </TouchableOpacity>
                </KeyboardAwareScrollView>
            </View>
        </LinearGradient>
    )
}

const styles = StyleSheet.create({
    root: {
        flex: 1,
        padding: 20
    },

    title: {
        textAlign: 'center',
        fontWeight: '700',
        fontSize: 30
    },
    codeFieldRoot: {
        marginTop: 20,
    },
    cell: {
        width: actuatedNormalize(48),
        height: actuatedNormalize(56),
        lineHeight: 48,
        fontSize: 24,
        borderWidth: 1.5,
        borderRadius: 10,
        borderColor: '#00000030',
        textAlign: 'center',
    },
    focusCell: {
        borderColor: '#48A7FE',
        borderRadius: 10,
    },

    otpContainer: {
        alignItems: 'center',
        flexDirection: 'row',
    },

    txtSeparator: {
        ...Typography.h3,
        color: '#888888',
    },

    btnResendOtp: {
    },

    btnSubmit: {
        width: actuatedNormalize(200),
        height: actuatedNormalize(48),
        marginTop: actuatedNormalize(18),
        marginBottom: actuatedNormalize(18),
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },

    txtLogin: {
        ...Typography.h1,
        fontSize: actuatedNormalize(14),
        color: Colors.WHITE,
    },
})

