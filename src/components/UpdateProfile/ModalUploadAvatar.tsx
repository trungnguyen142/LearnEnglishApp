import { StyleSheet, Image, View, TouchableOpacity } from 'react-native'
import React from 'react'
import Modal from 'react-native-modal'
import VText from '@/common/VText'
import IconUpdateProfile from '@/assets/images/IconUpdateProfile'
import { actuatedNormalize, PADDING_BOTTOM } from '@/common/Metrics'
import ImagePicker from 'react-native-image-crop-picker'

interface ModalProps {
    isVisible: boolean
    onCloseModal?: () => void
    onPress?: () => void
    handleUpLoad: any
}

const ModalUploadAvatar = (props: ModalProps) => {
    const { isVisible, onCloseModal, onPress, handleUpLoad } = props

    const openCamera = () => {
        ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: true
        }).then(image => {
            handleUpLoad(image)
        })

    }

    const openPicker = () => {
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            mediaType: 'photo',
            multiple: false,
            cropping: true,
        }).then(image => {
            handleUpLoad(image)
        })
    }

    return (
        <Modal
            isVisible={isVisible}
            onBackdropPress={onCloseModal}
            backdropOpacity={0.1}
            useNativeDriver={true}
            useNativeDriverForBackdrop={true}
            hideModalContentWhileAnimating={true}
            style={{
                margin: 0,
                justifyContent: 'flex-end'
            }}
        >
            <View style={{
                backgroundColor: 'white',
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
                paddingTop: 24,
                paddingHorizontal: 56,
                paddingBottom: PADDING_BOTTOM + 50
            }}>
                <VText textStyle={{
                    textAlign: 'center',
                    fontSize: 18
                }} >
                    Chọn ảnh đại diện
                </VText>
                <TouchableOpacity
                    onPress={openCamera}
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        alignSelf: 'center',
                        marginTop: 24
                    }}>
                    <Image source={IconUpdateProfile.camera} style={{
                        width: actuatedNormalize(40),
                        height: actuatedNormalize(40),
                    }} />
                    <VText textStyle={{
                        paddingLeft: 8,
                    }} >
                        Chụp ảnh mới từ camera
                    </VText>
                </TouchableOpacity>

                <View style={{
                    borderBottomWidth: 1,
                    marginVertical: 18
                }} />

                <TouchableOpacity
                    onPress={openPicker}
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        alignSelf: 'center',
                    }}>
                    <Image source={IconUpdateProfile.gallery} style={{
                        width: actuatedNormalize(40),
                        height: actuatedNormalize(40),
                    }} />
                    <VText textStyle={{
                        paddingLeft: 8,
                    }} >
                        Chọn ảnh trong thư viện
                    </VText>
                </TouchableOpacity>
            </View>
        </Modal>
    )
}

export default ModalUploadAvatar

const styles = StyleSheet.create({})