import { View, Text, SafeAreaView, TouchableOpacity, StyleSheet, Image, ImageBackground } from 'react-native'
import React, { useState } from 'react'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { actuatedNormalize } from '@/common/Metrics'
import { useDispatch } from 'react-redux'
import { Typography } from '@/common/FontStyles'
import ProfileImages from '@/assets/images/IconUpdateProfile'
import InputField from '@/common/InputField'
import IconLogin from '@/assets/images/InputIcon'
import CardView from 'react-native-cardview'
import FlexRow from '@/common/FlexRow'
import Icon from 'react-native-vector-icons/Ionicons'
import ModalChangeClass from '@/common/ModalChangeClass'
import BgButton from '@/assets/images/BgButton'
import ModalUploadAvatar from './ModalUploadAvatar'
import LinearGradient from 'react-native-linear-gradient'
import VText from '@/common/VText'
import Colors from '@/common/Colors'

export const UpdateProfile = () => {
    const dispatch = useDispatch()
    const [fullName, setFullName] = useState('')
    const [avatar, setAvatar] = useState(null)
    const [openChooseClass, setOpenChooseClass] = useState(false)
    const [openUpdateAvatar, setOpenUpdateAvatar] = useState(false)
    const [currentClass, setCurrentClass] = useState(0)
    const [sex, setSex] = useState(1)

    const handleUpdate = () => {

    }

    const onUploadImage = (image: any) => {
        setOpenUpdateAvatar(false)
        setAvatar(image.path)
        const mapPhoto: any = {
            uri: image?.path?.replace('file://', ''),
            name: image?.filename,
            type: image?.mime,
        }
        const form = new FormData()
        form.append('file', mapPhoto)
        // uploadAvatar(form)(dispatch)
        //     .then(res => {
        //         getUserInfor()
        //     })
        //     .catch(err => {
        //         Alert.alert('Upload ảnh thất bại!')
        //     })
    }

    return (
        <LinearGradient
            colors={['#4DA1FE', '#1FC7FB']}
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            style={{ flex: 1 }}
        >
            <SafeAreaView>
                <VText fontWeight='700' textStyle={{
                    color: Colors.WHITE,
                    fontSize: 22,
                    textAlign: 'center',
                }}>
                    Cập nhật thông tin cá nhân
                </VText>
            </SafeAreaView>
            <View style={{
                flex: 1,
                backgroundColor: Colors.WHITE_2,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
                marginTop: 20,
                padding: 16
            }}>
                <KeyboardAwareScrollView>
                    <View style={{
                        width: actuatedNormalize(178),
                        height: actuatedNormalize(178),
                        borderRadius: 200,
                        backgroundColor: '#D2E9FF',
                        alignItems: 'center',
                        justifyContent: 'center',
                        alignSelf: 'center',
                    }}>
                        <View style={{
                            backgroundColor: '#BBDFFF',
                            width: actuatedNormalize(150),
                            height: actuatedNormalize(150),
                            borderRadius: 200,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                            {
                                !avatar ?
                                    <TouchableOpacity
                                        onPress={() => setOpenUpdateAvatar(true)}
                                        activeOpacity={0.5}
                                        style={{
                                            backgroundColor: 'white',
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                            width: actuatedNormalize(110),
                                            height: actuatedNormalize(110),
                                            borderRadius: 200,
                                        }}>
                                        <Image source={ProfileImages.picture} style={{
                                            width: actuatedNormalize(47),
                                            height: actuatedNormalize(43)
                                        }} />
                                        <VText
                                            textStyle={{
                                                color: '#89A6C5',
                                                fontSize: 12,
                                                paddingTop: actuatedNormalize(8)
                                            }}
                                        >
                                            Tải ảnh lên
                                        </VText>
                                    </TouchableOpacity>
                                    :
                                    <TouchableOpacity
                                        onPress={() => setOpenUpdateAvatar(true)}
                                        activeOpacity={0.5}
                                    >
                                        <Image source={{ uri: avatar }} style={{
                                            width: actuatedNormalize(110),
                                            height: actuatedNormalize(110),
                                            borderRadius: 200,
                                        }} />
                                    </TouchableOpacity>
                            }

                        </View>
                    </View>
                    <InputField
                        onChangeText={(text: string) => setFullName(text)}
                        placeholder='Họ tên'
                        placeholderTextColor='#89A6C5'
                        containerStyle={{ marginTop: 16 }}
                        isLeftIcon={true}
                        leftIcon={fullName.length ? IconLogin.Account_blue : IconLogin.Account}
                    // onFocus={() => setWarningInput(false)}
                    />
                    <TouchableOpacity
                        activeOpacity={0.5}
                        onPress={() => setOpenChooseClass(true)}
                    >
                        <CardView
                            cardElevation={1}
                            cardMaxElevation={1}
                            cornerRadius={5}
                            style={{
                                backgroundColor: '#FFFFFF',
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                                width: '100%',
                                height: actuatedNormalize(48),
                                borderRadius: actuatedNormalize(40),
                                paddingHorizontal: actuatedNormalize(16),
                                marginTop: 24
                            }}
                        >
                            <FlexRow>
                                <Image source={IconLogin.teacher} style={{ width: actuatedNormalize(24), height: actuatedNormalize(24) }} />
                                <VText textStyle={{
                                    color: currentClass > 0 ? '#333333' : '#89A6C5',
                                    paddingLeft: 8
                                }}>
                                    {currentClass < 1 ? 'Chọn lớp' : `Lớp ${currentClass}`}
                                </VText>
                            </FlexRow>
                            <Icon name='chevron-down' size={24} color='#89A6C5' />
                        </CardView>
                    </TouchableOpacity>
                    <View style={{
                        marginVertical: 24
                    }}>
                        <VText>
                            Giới tính
                        </VText>
                        <FlexRow style={{
                            justifyContent: 'space-evenly',
                            paddingTop: actuatedNormalize(8)
                        }}>
                            <TouchableOpacity
                                onPress={() => setSex(1)}
                                style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                }}>
                                <View style={{
                                    width: actuatedNormalize(32),
                                    height: actuatedNormalize(32),
                                    borderRadius: 8,
                                    borderWidth: 2,
                                    borderColor: sex === 1 ? '#48A7FE' : '#89A6C5',
                                    alignItems: 'center',
                                    justifyContent: 'center'
                                }}>
                                    {
                                        sex === 1 &&
                                        <View style={{
                                            width: actuatedNormalize(19),
                                            height: actuatedNormalize(19),
                                            borderRadius: 20,
                                            backgroundColor: '#48A7FE'
                                        }} />
                                    }
                                </View>
                                <VText textStyle={{
                                    paddingLeft: 16
                                }}>
                                    Nam
                                </VText>
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => setSex(2)}
                                style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                }}>
                                <View style={{
                                    width: actuatedNormalize(32),
                                    height: actuatedNormalize(32),
                                    borderRadius: 8,
                                    borderWidth: 2,
                                    borderColor: sex === 2 ? '#48A7FE' : '#89A6C5',
                                    alignItems: 'center',
                                    justifyContent: 'center'
                                }}>
                                    {
                                        sex === 2 &&
                                        <View style={{
                                            width: actuatedNormalize(19),
                                            height: actuatedNormalize(19),
                                            borderRadius: 20,
                                            backgroundColor: '#48A7FE'
                                        }} />
                                    }
                                </View>
                                <VText textStyle={{
                                    paddingLeft: 16
                                }}>
                                    Nữ
                                </VText>
                            </TouchableOpacity>
                        </FlexRow>
                    </View>
                    <TouchableOpacity
                        activeOpacity={0.5}
                        onPress={() => handleUpdate()}
                    >
                        <ImageBackground
                            style={styles.btnSubmit}
                            resizeMode='contain'
                            source={BgButton.bg_button_blue}>
                            <Text style={styles.txtLogin}>
                                Xác nhận
                            </Text>
                        </ImageBackground>
                    </TouchableOpacity>
                </KeyboardAwareScrollView>
            </View>
            <ModalChangeClass
                isVisible={openChooseClass}
                onCloseModal={() => setOpenChooseClass(false)}
                onPress={(value: number) => {
                    setCurrentClass(value)
                    setOpenChooseClass(false)
                }}
            />
            <ModalUploadAvatar
                isVisible={openUpdateAvatar}
                onCloseModal={() => setOpenUpdateAvatar(false)}
                handleUpLoad={(image: any) => onUploadImage(image)}
            />
        </LinearGradient>
    )
}


const styles = StyleSheet.create({
    btnSubmit: {
        width: actuatedNormalize(200),
        height: actuatedNormalize(48),
        marginTop: actuatedNormalize(18),
        marginBottom: actuatedNormalize(18),
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },

    txtLogin: {
        ...Typography.h1,
        fontSize: actuatedNormalize(14),
        color: Colors.WHITE,
    },
})
