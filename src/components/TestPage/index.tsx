import { View, SafeAreaView, ScrollView } from 'react-native'
import React, { useState } from 'react'
import SpaceBetween from '@/common/SpaceBetween'
import FlexRow from '@/common/FlexRow'
import GradientButton from '@/common/GradientButton'
import IButton from '@/common/IButton'
import VText from '@/common/VText'
import { fontWeight } from '@/common/Metrics'
import Images from '@/assets/images/HomeIcon'
import styles from './styles'
import Header from '@/common/Header'

export const TestPage = () => {
    return (
        <SafeAreaView style={styles.container}>
            <Header />
            <ScrollView style={styles.body}>

            </ScrollView>
        </SafeAreaView>
    )
}
