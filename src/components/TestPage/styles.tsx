import { StyleSheet } from 'react-native'
import Colors from '@/common/Colors'
import { actuatedNormalize } from '@/common/Metrics'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.WHITE
    },

    body: {
        flex: 1,
        backgroundColor: Colors.WHITE_2,
        padding: actuatedNormalize(16),
    },

    imageItem: {
        width: '100%',
        height: actuatedNormalize(144),
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
    },

})

export default styles