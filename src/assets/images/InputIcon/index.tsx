const Images = {
    Account: require('./account.png'),
    Account_red: require('./account_red.png'),
    Account_blue: require('./account_blue.png'),
    Password: require('./security.png'),
    Password_red: require('./security_red.png'),
    Password_blue: require('./security_blue.png'),
    teacher: require('./teacher.png'),

}

export default Images