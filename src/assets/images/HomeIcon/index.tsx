const Images = {
    notifi: require('./Notifi.png'),
    star: require('./medal_star.png'),
    note: require('./note.png'),
    test: require('./test.png'),
    card_blue: require('./card_blue.png'),
    card_yellow: require('./card_yellow.png'),
    luyentap: require('./luyentap.png'),
    ontap_kienthuc: require('./ontap_kienthuc.png'),

}

export default Images