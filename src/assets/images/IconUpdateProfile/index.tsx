const Images = {
    camera: require('./camera.png'),
    gallery: require('./gallery.png'),
    picture: require('./picture.png'),
    camera_blue: require('./camera_blue.png'),
    edit_blue: require('./edit_blue.png'),
    call_add: require('./call_add.png'),
    shield: require('./shield.png'),
}

export default Images