const Images = {
    Home: require('./Home.png'),
    Home_Non_Active: require('./Home_Non_Active.png'),
    Test: require('./Test.png'),
    Test_Non_Active: require('./Test_Non_Active.png'),
    Personal: require('./Profile.png'),
    Personal_Non_Active: require('./Profile_Non_Active.png')

}

export default Images