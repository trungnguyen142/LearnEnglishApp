import React from 'react'
import { LogBox, StatusBar } from 'react-native'
import { WRootToastApp } from 'react-native-smart-tip'
import { NavigationContainer } from '@react-navigation/native'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/es/integration/react'
import { store, persistor } from './src/store/index'
import AppNavigator from '@/components/Navigation/AppNavigator'
import "react-native-gesture-handler"

const App = () => {

  LogBox.ignoreAllLogs()
  console.warn = () => { }

  return (
    <Provider store={store}>
      <PersistGate persistor={persistor()}>
        <NavigationContainer>
          <WRootToastApp>
            <StatusBar barStyle='dark-content' />
            <AppNavigator />
          </WRootToastApp>
        </NavigationContainer>
      </PersistGate>
    </Provider>
  )
}


export default App